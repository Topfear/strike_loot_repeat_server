import socket

from game_objects.players_group import PlayersGroup
from settings import Settings


class Network:
    """ Класс для работы с сетью """

    def __init__(self) -> None:
        """ Инициализация работы с сетью """
        self.sock = self.create_main_socket()

    def create_main_socket(self) -> socket.socket:
        """ Создание главного сокета для коннекта игроков """
        main_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        main_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
        ip = socket.gethostbyname(socket.gethostname())
        port = Settings.SERVER_PORT
        print(f"Server started at {ip}:{port}")
        main_socket.bind((ip, port))
        main_socket.setblocking(False)
        main_socket.listen(5)
        return main_socket

    def check_new_connections(self, players: PlayersGroup) -> None:
        """ Проверка желающих подключиться к игре """
        try:
            new_socket, addr = self.sock.accept()
            print("Connected", addr)
            new_socket.setblocking(False)
            players.add(new_socket, addr)
        except BlockingIOError:
            pass
        # TODO: возвращать new_connection или None

    def close(self) -> None:
        """ Закрытие Network сокета """
        self.sock.close()
