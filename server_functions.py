import os

import pygame

from game_objects.bullet_group import BulletsGroup
from game_objects.players_group import PlayersGroup
from network import Network
from settings import Settings


def check_quit(network: Network) -> None:
    """ Проверка выхода из игры """
    if Settings.DISPLAY:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                network.close()
                os._exit(0)


def draw_room(screen: pygame.surface.Surface, players: PlayersGroup, bullets: BulletsGroup) -> None:
    """ Нарисовать изображение комнаты """
    screen.fill("BLACK")
    players.draw(screen)
    bullets.draw(screen)
    pygame.display.update()
