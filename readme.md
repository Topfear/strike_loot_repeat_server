## Сервер для игры "Strike, loot, repeat"

Сервер для мультиплеерной игры. Одновременно могут играть до 8 человек.     
Среди игроков есть боты под контролем AI (разной степени интелекта).
Здесь можно скачать клиент для игры: ссылка


## Установка

WIP (work in progress)

## MVP Checklist

1. Игровая карта 2000х2000 пикселей
2. Игроки могут перемещаться по карте
3. Игроки видят друг друга, видят направление взгляда соперников
4. Игроки могут стрелять пулями
5. При попадании пулей в соперника он исчезает и появляется в другом месте, игрок получает 1 очко
6. В правом верхнем углу отображается таблица очков
