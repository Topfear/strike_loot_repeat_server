import random
from socket import socket

from pygame.sprite import Group
from pygame.surface import Surface

from game_objects.bullet_group import BulletsGroup
from game_objects.player import Player


class PlayersGroup(Group):
    """ Группа игроков """

    def __init__(self, bullets: BulletsGroup) -> None:
        """ Инициализация группы игроков """
        super().__init__()
        self.players_ids = [x for x in range(8)]
        self.bullets = bullets

    def add(self, new_socket: socket = None, addr: tuple = None) -> None:  # type: ignore[override]
        """ Добавить игрока в группу """
        if new_socket and addr:
            player = Player(
                new_socket, addr, self.get_random_id(),
                self.bullets,
            )
            super().add(player)

    def remove(self, player: Player) -> None:   # type: ignore[override]
        """ Удалить игрока из группы """
        self.players_ids.append(player.player_id)
        super().remove(player)

    def sprites(self) -> list[Player]:  # type: ignore[override]
        """ Спрайты группы """
        return super().sprites()        # type: ignore[return-value]

    def get_random_id(self) -> int:
        """ Получить случайный id """
        player_id = random.choice(self.players_ids)
        self.players_ids.remove(player_id)
        return player_id

    def check_visible_objects(self, bullets: BulletsGroup) -> None:
        """
        Вычисление кого/что видит игрок
        и добавление в список того что видит игрок
        """
        for player_1 in self.sprites():
            # очищаем списки для следующей итерации
            player_1.visible_objects = []
            player_1.visible_players = []
            player_1.visible_bullets = []

            self._check_visible_players(player_1)
            self._check_visible_bullets(player_1, bullets)

    def handle_players_inputs(self) -> None:
        """ Считываем команды игроков и обрабатываем команды игроков """
        for player in self.sprites():
            player.handle_player_input()
            player.update()

    def send_game_state(self) -> None:
        """ Отправить новое состояние игрового поля """
        for player in self.sprites():
            try:
                player.send_game_state()
                player.errors = 0
            except ConnectionResetError:
                player.errors += 1

    def disconnect_irresponsive_players(self) -> None:
        """ Чистим список от отвалившихся игроков """
        for player in self.sprites():
            if player.errors >= 500:
                player.conn.close()
                self.remove(player)
                print("Player disconected")

    def send_game_state_or_disconnect(self, bullets: BulletsGroup) -> None:
        """ Отослать новое состояние игры или отключить нереагирующих игроков """
        self.check_visible_objects(bullets)
        self.send_game_state()
        self.disconnect_irresponsive_players()

    def draw(self, surface: Surface) -> None:   # type: ignore[override]
        """ Рисует всех игроков """
        for sprite in self.sprites():
            sprite.blitme(surface)

    def _check_visible_players(self, player_1: Player) -> None:
        for player_2 in self.sprites():
            if player_1.player_id != player_2.player_id:
                # player_1 видит player_2
                player_1.check_visible(player_2)

    def _check_visible_bullets(self, player_1: Player, bullets: BulletsGroup) -> None:
        for bullet in bullets.sprites():
            player_1.check_visible_bullet(bullet)
