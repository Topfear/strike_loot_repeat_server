from __future__ import annotations
import json
import math
import random
import re
from socket import socket

import pygame
from pygame.surface import Surface
from pygame.sprite import Sprite
from game_objects.bullet import Bullet
from game_objects.bullet_group import BulletsGroup

from settings import Settings
from utils.datatypes import PlayerMessage, Point, RelativePosition, Speed
from utils.geometry import two_points_distance


class Player(Sprite):
    """ Игрок """

    def __init__(self, conn: socket, addr: tuple,
                 player_id: int, bullets: BulletsGroup):
        """ Инициализировать игрока """
        super().__init__()
        self.bullets = bullets

        # соединение с игроком
        self.conn = conn
        self.addr = addr
        self.player_id = player_id

        # при необходимости можно задать атрибуты
        self._respawn()
        self.r = Settings.PLAYER_SIZE
        self.color = Settings.COLORS[player_id]
        self.x_float: float = self.x
        self.y_float: float = self.y

        # ошибки при дисконекте
        self.errors = 0

        # область видимости игрока
        self.w_vision = 1000
        self.h_vision = 800

        # скорость игрока
        self.abs_speed = Settings.PLAYER_SPEED
        self.speed_x = 0
        self.speed_y = 0

        # угол поворота
        self.angle = 0

        # объекты которые видит игрок
        self.visible_objects: list = []
        self.visible_players: list[Player] = []
        self.visible_bullets: list[Bullet] = []
        self.send_init_state()

        # стрельба
        self._reload_frames = Settings.FRAMES_TO_RELOAD
        self._reload_counter = Settings.FRAMES_TO_RELOAD

    @property
    def _is_loaded(self) -> bool:
        if self._reload_counter == self._reload_frames:
            return True
        return False

    @property
    def top(self) -> Point:
        """ Координаты верхней стороны игрока """
        return Point(pos_x=self.x, pos_y=self.y - self.r)

    @property
    def bottom(self) -> Point:
        """ Координаты нижней стороны игрока """
        return Point(pos_x=self.x, pos_y=self.y + self.r)

    @property
    def left(self) -> Point:
        """ Координаты левой стороны игрока """
        return Point(pos_x=self.x - self.r, pos_y=self.y)

    @property
    def right(self) -> Point:
        """ Координаты правой стороны игрока """
        return Point(pos_x=self.x + self.r, pos_y=self.y)

    @property
    def center(self) -> Point:
        """ Координаты центра """
        return Point(pos_x=self.x, pos_y=self.y)

    @property
    def sight_direction(self) -> Point:
        """ Точка на окружности в направлении которой смотрит игрок """
        x_angled = int(self.center.pos_x - math.cos(math.radians(self.angle - 90)) * self.r)
        y_angled = int(self.center.pos_y + math.sin(math.radians(self.angle - 90)) * self.r)
        return Point(x_angled, y_angled)

    def update(self) -> None:      # type: ignore[override]
        """ Обновление координат игрока и вычисление коллизий """
        super().update()
        speed = self.get_speed()
        speed = self.handle_room_borders(speed)
        speed = self.check_players_collision(speed)
        self.apply_speed(speed)
        self._check_bullet_collision()
        self._fire()
        self._do_reload()

    def get_speed(self) -> Speed:
        """ Получить скорость игрока """
        return Speed(x_direction=self.speed_x, y_direction=self.speed_y)

    def apply_speed(self, speed: Speed) -> None:
        """ Утвердить скорость игрока """
        self.x_float += speed.x_direction
        self.y_float += speed.y_direction
        self.x = int(self.x_float)
        self.y = int(self.y_float)

    def handle_room_borders(self, speed: Speed) -> Speed:
        """ Столкновение с краями комнаты """
        speed = self.handle_left_right_room_borders(speed)
        speed = self.handle_top_bottom_room_borders(speed)
        return speed

    def handle_left_right_room_borders(self, speed: Speed) -> Speed:
        """ Cтолкновение с левым и правым краем комнаты """
        out_of_left_border = self.left.pos_x <= 0
        out_of_right_border = self.right.pos_x >= Settings.WIDTH_ROOM
        left_border_and_going_left = out_of_left_border and speed.is_directed_left
        right_border_and_going_right = out_of_right_border and speed.is_directed_right

        if left_border_and_going_left or right_border_and_going_right:
            speed.x_direction = 0
        return speed

    def handle_top_bottom_room_borders(self, speed: Speed) -> Speed:
        """ Столкновение с верхом и низом комнаты """
        out_of_top_border = self.top.pos_y <= 0
        out_of_bottom_border = self.bottom.pos_y >= Settings.HEIGHT_ROOM
        top_border_and_going_top = out_of_top_border and speed.is_directed_up
        bottom_border_and_going_bottom = out_of_bottom_border and speed.is_directed_down

        if top_border_and_going_top or bottom_border_and_going_bottom:
            speed.y_direction = 0
        return speed

    def check_players_collision(self, speed: Speed) -> Speed:
        """ Проверка столкновения с другими игроками """
        for visible_player in self.visible_players:
            distance = two_points_distance(self.center, visible_player.center)
            players_radius = self.r + visible_player.r
            is_collided = distance <= players_radius

            if is_collided:
                speed = self.handle_players_collision(speed, visible_player)
        return speed

    def handle_players_collision(self, speed: Speed, another_player: Player) -> Speed:
        """ Обработка столкновения с другим игроком """
        another_player_pos = self.relative_position(another_player.center)
        speed = self.handle_left_right_player_collision(speed, another_player_pos)
        speed = self.handle_top_bottom_player_collision(speed, another_player_pos)
        return speed

    def handle_left_right_player_collision(
            self, speed: Speed, another_player_pos: RelativePosition) -> Speed:
        """ Столкновение с левой или правой стороной игрока """
        if another_player_pos.is_left and speed.is_directed_left:
            speed.x_direction = 0
        if another_player_pos.is_right and speed.is_directed_right:
            speed.x_direction = 0
        return speed

    def handle_top_bottom_player_collision(
            self, speed: Speed, another_player_pos: RelativePosition) -> Speed:
        """ Столкновение с верхней или нижней стороной игрока """
        if another_player_pos.is_above and speed.is_directed_up:
            speed.y_direction = 0
        if another_player_pos.is_under and speed.is_directed_down:
            speed.y_direction = 0
        return speed

    def parse(self, data: str) -> list | None:
        """ Проанализировать сообщение игрока """
        match = re.search(r"\<(.*?)\>", data)
        if match:
            return json.loads(match.group(1))
        return None

    def get_player_data(self) -> str:
        """ Получить информацию игрока """
        try:
            data_bytes = self.conn.recv(1024)
            data = data_bytes.decode()
        except (BlockingIOError, ConnectionResetError):
            data = ""
        return data

    def set_player_data(self, player_data: str) -> None:
        """ Изменить параметры игрока в соответствии с командой игрока """
        parsed_data = self.parse(player_data)

        if parsed_data:
            player_info = PlayerMessage(*parsed_data)
            self.change_speed(player_info)
            self.change_angle(player_info)

    def handle_player_input(self) -> None:
        """ Обработать ввод игрока """
        player_data = self.get_player_data()
        self.set_player_data(player_data)

    def change_speed(self, player_info: PlayerMessage) -> None:
        """ Изменить скорость игрока """
        if player_info:
            speed_x, speed_y = player_info.x_speed, player_info.y_speed

            if (speed_x == 0) and (speed_y == 0):
                self.speed_x = 0
                self.speed_y = 0
            else:
                self.speed_x, self.speed_y = self.normalize(
                    speed_x, speed_y)

    def change_angle(self, player_info: PlayerMessage) -> None:
        """ Изменить направление взгляда игрока """
        self.angle = player_info.angle

    def check_visible(self, another_player: Player) -> None:
        """
        Вычисление кого/что видит игрок
        и добавление в список того что видит игрок
        """
        dist_x = another_player.x_float - self.x_float
        dist_y = another_player.y_float - self.y_float

        if (abs(dist_x) <= self.w_vision // 2 + another_player.r) \
                or (abs(dist_y) <= self.h_vision // 2 + another_player.r):
            x_pos = round(dist_x)
            y_pos = round(dist_y)
            radius = round(another_player.r)
            player_id = another_player.player_id
            angle = another_player.angle

            other_player_info = {
                "type": "p",
                "x": x_pos,
                "y": y_pos,
                "r": radius,
                "id": player_id,
                "angle": angle,
            }
            self.visible_objects.append(other_player_info)
            self.visible_players.append(another_player)

    def check_visible_bullet(self, bullet: Bullet) -> None:
        """ Проверка видимости пули """
        dist_x = bullet.position_x - self.x_float
        dist_y = bullet.position_y - self.y_float

        if (abs(dist_x) <= self.w_vision // 2 + bullet.radius) \
                or (abs(dist_y) <= self.h_vision // 2 + bullet.radius):
            x_pos = round(dist_x)
            y_pos = round(dist_y)
            radius = round(bullet.radius)

            other_player_info = {
                "type": "b",
                "x": x_pos,
                "y": y_pos,
                "r": radius,
            }
            self.visible_objects.append(other_player_info)
            self.visible_bullets.append(bullet)

    def relative_position(self, point: Point) -> RelativePosition:
        """ Позиция точки относительно игрока """
        is_left = self.center.pos_x - point.pos_x > 0
        is_right = self.center.pos_x - point.pos_x < 0
        is_above = self.center.pos_y - point.pos_y > 0
        is_under = self.center.pos_y - point.pos_y < 0
        return RelativePosition(is_above, is_under, is_left, is_right)

    def normalize(self, speed_x: float, speed_y: float) -> tuple:
        """
        Нормализовать скорость игрока.
        Чтобы по диагонали игрок не двигался быстрее чем по прямой
        """
        length_v = (speed_x ** 2 + speed_y ** 2) ** 0.5
        speed_x, speed_y = (speed_x / length_v, speed_y / length_v)
        speed_x, speed_y = (speed_x * self.abs_speed, speed_y * self.abs_speed)
        return speed_x, speed_y

    # TODO: сделать единый тип передачи сообщения
    def pack_message(self, obj: object) -> bytes:
        """ Запаковывает сообщение для игрока """
        message = json.dumps(obj)
        message = f"<{message}>"
        return message.encode()

    def pack_game_state(self) -> bytes:
        """ Запаковывает информацию об игре для игрока """
        message = {
            "x_pos": self.center.pos_x,
            "y_pos": self.center.pos_y,
            "objects": self.visible_objects,
        }
        return self.pack_message(message)

    def send_game_state(self) -> None:
        """ Отослать состояние игры игроку """
        msg = self.pack_game_state()
        self._send_msg(msg)

    def send_init_state(self) -> None:
        """ Отослать начальное состояние игры игроку """
        data = {
            "player_id": self.player_id,
            "radius": self.r,
            "room_width": Settings.WIDTH_ROOM,
            "room_hight": Settings.HEIGHT_ROOM,
        }
        message = self.pack_message(data)
        self._send_msg(message)

    def blitme(self, screen: Surface) -> None:
        """ Рисует игрока на экране """
        x_pos = round(self.center.pos_x * Settings.WIDTH_PROPORTIONS)
        y_pos = round(self.center.pos_y * Settings.HEIGHT_PROPORTIONS)
        radius = round(self.r * Settings.WIDTH_PROPORTIONS)
        pygame.draw.circle(screen, self.color, (x_pos, y_pos), radius)
        x_angled = x_pos - math.cos(math.radians(self.angle - 90)) * radius * 2
        y_angled = y_pos + math.sin(math.radians(self.angle - 90)) * radius * 2

        pygame.draw.line(screen, "RED", (x_pos, y_pos), (x_angled, y_angled), 2)

    def _do_reload(self) -> None:
        if self._reload_counter != self._reload_frames:
            self._reload_counter += 1

    def _reload(self) -> None:
        self._reload_counter = 0

    def _fire(self) -> None:
        if self._is_loaded:
            self.bullets.spawn_bullet(self.center, self.sight_direction)
            self._reload()

    def _check_bullet_collision(self) -> None:
        for visible_bullet in self.visible_bullets:
            distance = two_points_distance(self.center, visible_bullet.center)
            player_bullet_radius = self.r + visible_bullet.radius
            is_collided = distance <= player_bullet_radius

            if is_collided:
                visible_bullet.kill()
                self._respawn()

    def _respawn(self) -> None:
        self.x = random.randint(0, Settings.WIDTH_ROOM)
        self.y = random.randint(0, Settings.HEIGHT_ROOM)
        self.x_float = float(self.x)
        self.y_float = float(self.x)

    def _send_msg(self, message: bytes) -> None:
        try:
            self.conn.send(message)
        except BrokenPipeError:
            self.errors += 1
