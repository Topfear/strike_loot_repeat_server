import pygame
from pygame.sprite import Sprite
from pygame.surface import Surface
from settings import Settings

from utils.datatypes import Point, Speed


class Bullet(Sprite):
    """ Пуля """

    def __init__(self, point_1: Point, point_2: Point) -> None:
        """ Инициализация пули """
        super().__init__()

        self.radius = Settings.BULLET_SIZE
        self.absolute_speed = Settings.BULLET_SPEED
        self.max_travel_distance = Settings.BULLET_TRAVEL_DISTANCE
        self.speed = self.calculate_speed(point_1, point_2)
        self.traveled_distance = 0
        self.need_remove = False
        self.position_x = point_2.pos_x
        self.position_y = point_2.pos_y
        self.color = (255, 255, 255)

    @property
    def center(self) -> Point:
        """ Координаты центра """
        return Point(pos_x=int(self.position_x), pos_y=int(self.position_y))

    def calculate_speed(self, point_1: Point, point_2: Point) -> Speed:
        """ Вычисление скорости пули """
        speed_x = point_2.pos_x - point_1.pos_x
        speed_y = point_2.pos_y - point_1.pos_y
        length_vector = (speed_x ** 2 + speed_y ** 2) ** 0.5
        speed_x, speed_y = (speed_x / length_vector, speed_y / length_vector)
        speed_x, speed_y = (speed_x * self.absolute_speed, speed_y * self.absolute_speed)
        return Speed(speed_x, speed_y)

    def update(self) -> None:   # type: ignore[override]
        """ Обновить позицию пули """
        self.position_x += self.speed.x_direction
        self.position_y += self.speed.y_direction
        self.traveled_distance += self.absolute_speed

        if self.traveled_distance >= self.max_travel_distance:
            self.need_remove = True

    def blitme(self, screen: Surface) -> None:
        """ Рисует пулю на экране """
        x_pos = round(self.position_x * Settings.WIDTH_PROPORTIONS)
        y_pos = round(self.position_y * Settings.HEIGHT_PROPORTIONS)
        radius = round(self.radius * Settings.WIDTH_PROPORTIONS)
        pygame.draw.circle(screen, self.color, (x_pos, y_pos), radius)
