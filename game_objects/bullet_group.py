from pygame.sprite import Group
from pygame.surface import Surface
from game_objects.bullet import Bullet

from utils.datatypes import Point


class BulletsGroup(Group):
    """ Группа пуль """

    def sprites(self) -> list[Bullet]:  # type: ignore[override]
        """ Спрайты группы """
        return super().sprites()        # type: ignore[return-value]

    def spawn_bullet(self, point_1: Point, point_2: Point) -> None:
        """ Создать пулю """
        bullet = Bullet(point_1, point_2)
        self.add(bullet)

    def update_bullets(self) -> None:
        """ Обновляет пули, удаляет ненужные пули """
        self.update()

        for bullet in self.sprites():
            if bullet.need_remove:
                self.remove(bullet)

    def draw(self, surface: Surface) -> None:   # type: ignore[override]
        """ Рисует все пули """
        for bullet in self.sprites():
            bullet.blitme(surface)
