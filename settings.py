

class Settings():
    """ Настройки сервера """

    WIDTH_ROOM = 2000
    HEIGHT_ROOM = 2000

    WIDTH_SERVER_WINDOW = 300
    HEIGHT_SERVER_WINDOW = 300

    WIDTH_PROPORTIONS = WIDTH_SERVER_WINDOW / WIDTH_ROOM
    HEIGHT_PROPORTIONS = HEIGHT_SERVER_WINDOW / HEIGHT_ROOM

    FPS = 60

    # настройки игрока
    PLAYER_SIZE = 50
    PLAYER_SPEED = 5

    # настройки пули
    BULLET_TRAVEL_DISTANCE = PLAYER_SIZE * 10
    BULLET_SPEED = 15
    BULLET_SIZE = PLAYER_SIZE // 10
    BULLETS_MAX_COUNT = PLAYER_SIZE // 5

    COLORS = {
        0: (200, 200, 0),
        1: (200, 0, 200),
        2: (0, 100, 200),
        3: (100, 60, 200),
        4: (100, 60, 100),
        5: (255, 255, 0),
        6: (255, 123, 0),
        7: (123, 255, 50),
    }

    SERVER_IP = "192.168.1.104"
    SERVER_PORT = 10_000

    FRAMES_TO_RELOAD = 30
    DISPLAY = False
