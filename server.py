import pygame
from game_objects.bullet_group import BulletsGroup
from game_objects.players_group import PlayersGroup

from network import Network
from server_functions import check_quit, draw_room
from settings import Settings


class Game:
    """ Серверная часть игры """

    def __init__(self) -> None:
        """ Инициализация игры и игровых сущностей """
        self.network = Network()

        # игровое окно сервера
        pygame.init()
        if Settings.DISPLAY:
            self.screen = pygame.display.set_mode((
                Settings.WIDTH_SERVER_WINDOW,
                Settings.HEIGHT_SERVER_WINDOW))
        self.clock = pygame.time.Clock()

        # игровые объекты
        self.bullets = BulletsGroup()
        self.players = PlayersGroup(self.bullets)

    def run(self) -> None:
        """ Запуск игрового цикла """
        while True:
            self.network.check_new_connections(self.players)
            self.players.handle_players_inputs()
            self.bullets.update_bullets()
            self.players.send_game_state_or_disconnect(self.bullets)
            if Settings.DISPLAY:
                draw_room(self.screen, self.players, self.bullets)
            check_quit(self.network)
            self.clock.tick(Settings.FPS)


if __name__ == "__main__":
    game = Game()
    game.run()
