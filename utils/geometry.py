from utils.datatypes import Point


def two_points_distance(point_1: Point, point_2: Point) -> int:
    """ вычисление расстояния между двумя точками """
    x_distance = abs(point_1.pos_x - point_2.pos_x)
    y_distance = abs(point_1.pos_y - point_2.pos_y)
    distance = (x_distance ** 2 + y_distance ** 2) ** 0.5
    return int(distance)
