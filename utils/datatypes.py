from dataclasses import dataclass
from typing import NamedTuple


class Point(NamedTuple):
    """ Точка в двумерном пространстве """
    pos_x: int
    pos_y: int


class PlayerMessage(NamedTuple):
    """ Сообщение игрока """
    x_speed: int
    y_speed: int
    angle: int


@dataclass
class Speed:
    """ Скорость в двумерном пространстве """
    x_direction: int
    y_direction: int

    @property
    def is_directed_up(self) -> bool:
        """ Возвращает True если скорость направлена вверх """
        return self.y_direction < 0

    @property
    def is_directed_down(self) -> bool:
        """ Возвращает True если скорость направлена вниз """
        return self.y_direction > 0

    @property
    def is_directed_left(self) -> bool:
        """ Возвращает True если скорость направлена влево """
        return self.x_direction < 0

    @property
    def is_directed_right(self) -> bool:
        """ Возвращает True если скорость направлена вправо """
        return self.x_direction > 0

    @property
    def is_still(self) -> bool:
        """ Возвращает True если скорость равна 0 """
        return bool(self.x_direction or self.y_direction)


class RelativePosition(NamedTuple):
    """ Относительная позиция """
    is_above: bool
    is_under: bool
    is_left: bool
    is_right: bool
